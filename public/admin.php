<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != 'adminka' ||
    md5($_SERVER['PHP_AUTH_PW']) != md5('11232331')) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}
header('Content-Type: text/html; charset=UTF-8');
echo '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="admcss.css">
</head>
<body>';
echo 'Добро пожаловать в админку! Вы успешно авторизовались и видите защищенные паролем данные.';
$c_format = '<div class="col ctd">%s</div>';
$c_bwformat='<div class="col ctd bw">%s</div>';
$ch_format = '<div class="col cth">%s</div>';
$r_format = '<div class="row ctr">%s</div>';
$c_sup_format = '<div class="col ctd dirc">%s</div>';
$inputformat='<input name="%s" type="%s" value="%s" >';
$table_content='';
// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
$db = new PDO('mysql:host=localhost;dbname=u21044', 'u21044', '76898768');
$powers = $db->prepare('select s_rusdesc from superpower as s join app_sup as ap on s.sup_id = ap.sup_id where ap.id = ?');
$gen = $db->prepare('select rus_desc from gender where sex=?');
$lim = $db->prepare('select rus_desc from amount_of_limbs where limbs=?');
$change_table_string = '';
if($_SERVER['REQUEST_METHOD']=='POST'){
    $table_request;
    switch ($_POST['option']){
        case 'delete':
            if(!empty($_POST['select'])) {
                $table_request=$db->prepare('delete from application where id = ?');
                foreach($_POST['select'] as $val)
                $table_request->execute([$val]);
            }
            else if(isset($_POST['select_all'])){
                $db->query('truncate application');
            }
            break;
        case 'change':
            if(!empty($_POST['select']) or isset($_POST['select_all'])){
                $set_for_change=[];
                if(!empty($_POST['select'])) {
                    $table_request=$db->prepare('select  a.id, a.fio, u.login, a.email, a.birthdate, a.sex, a.limbs, \'sup\',a.biography,  u.passhash from application as a join users as u on a.id = u.id where a.id = ?');
                    foreach($_POST['select'] as $val){
                        $table_request->execute([$val]);
                        $set_for_change[''.$val] = $table_request->fetch(PDO::FETCH_ASSOC);
                    }
                }
                else{
                    if (isset($_POST['select_all'])){
                        $table_request=$db->prepare('select  a.id, a.fio, u.login, a.email, a.birthdate, a.sex, a.limbs, \'sup\',a.biography, u.passhash from application as a join users as u on a.id = u.id');
                        $table_request->execute();
                        $table_request=$table_request->fetchAll(PDO::FETCH_ASSOC);
                    }
                }


                $formates=[
                    'id'=>['tag'=>'%s','type'=>''],
                    'fio'=>['tag'=>'<input name="changes[fio[%s]]" type="text" value="%s">'],
                    'email'=>['tag'=>'<input name="changes[email[%s]]" type="text" value="%s">'],
                    'birthdate'=>['tag'=>'<input name="changes[birthdate[%s]]" type="date" value="%s">'],
                    'sex'=>['tag'=>'<select name="changes[sex[%s]]">%s</select>'],
                    'limbs'=>['tag'=>'<select name="changes[limbs[%s]]">%s</select>'],
                    'biography'=>['tag'=>'<textarea name="changes[biography[%s]]">%s</textarea>'],
                    'login'=>['tag'=>'<input name="changes[login[%s]]" type="text" value="%s">'],
                    'passhash'=>['tag'=>'<textarea name="changes[passhash[%s]]">%s</textarea>'],
                    'sup'=> ['tag'=> '<select multiple name="changes[app_sup[%s]]">%s</select>']
                ];
                $gdr = $db->prepare('select * from gender');
                $gdr->execute();
                $gender = $gdr->fetchAll(PDO::FETCH_ASSOC);
                $lmbs = $db->prepare('select * from amount_of_limbs');
                $lmbs->execute();
                $limbs = $lmbs->fetchAll(PDO::FETCH_ASSOC);
                $sp = $db->prepare('select * from superpower');
                $sp->execute();
                $sup = $sp->fetchAll(PDO::FETCH_ASSOC);
                $sp = $db->prepare('select sup_id from app_sup where id = ?');
                foreach($set_for_change as $row) {
                    $row_string = '';
                    $gender_string = '';
                    $limbs_string = '';
                    foreach ($gender as $gndr) {
                        $gender_string .= sprintf('<option %s value="%s">%s</option>', ($gndr['sex'] == $row['sex']) ? 'selected' : '', $gndr['sex'], $gndr['rus_desc']);
                    }
                    foreach ($limbs as $l) {
                        $limbs_string .= sprintf('<option %s value="%s">%s</option>', ($l['limbs'] == $row['limbs']) ? 'selected' : '', $l['limbs'], $l['rus_desc']);
                    }
                    $supstr='';
                    $sp->execute([$row['id']]);
                    $s=$sp->fetchAll(PDO::FETCH_COLUMN);
                    foreach($sup as $spower){
                        $supstr .= sprintf('<option %s value="%s">%s</option>', (''.in_array($spower['sup_id'], $s)) ? 'selected' : '', $spower['sup_id'], $spower['s_rusdesc']);
                    }
                    $row['sex'] = $gender_string;
                    $row['limbs'] = $limbs_string;
                    $row['sup'] = $supstr;
                    foreach ($row as $key => $val) {
                        $row_string .= sprintf($c_format, sprintf($formates[$key]['tag'], ''.$row['id'],$val));
                    }
                    $change_table_string .= sprintf($r_format, $row_string);
                }
                    break;
                    }
                }
}

$usersset = $db->prepare('select a.id, a.fio as \'фио\', l.login, a.email, a.birthdate as \'дата рождения\', 
       a.sex as \'пол\',  a.limbs as \'кол-во конечностей\', 
      \'суперспособности\',a.biography  as \'биография\', l.passhash as \'хэш пароля\' from application as a left join users as l  on a.id = l.id');
$usersset->execute();




$headrow = '';
$row = $usersset->fetch(PDO::FETCH_ASSOC);
foreach($row as $colname => $val){
    $headrow.=sprintf($ch_format, $colname);
}
do{
    $gen->execute([$row['пол']]);
    $lim->execute([$row['кол-во конечностей']]);
    $gender = $gen->fetchColumn();
    $lims = $lim->fetchColumn();
    $powers->execute([$row['id']]);
    $supstring = '';
    $sup_arr = $powers->fetchAll(PDO::FETCH_COLUMN, 0);
    foreach ($sup_arr as $sup){
        $supstring.=sprintf($r_format, $sup);
    }
    $row['суперспособности'] = $supstring;
    $row['пол'] = $gender;
    $row['кол-во конечностей'] = $lims;
    $table_row_string = sprintf($c_format,'<input name="select['.$row['id'].']" type="checkbox" value="'.$row['id'].'">');
    foreach ($row as $colname => $val)
        $table_row_string.=sprintf($c_format, ($val));
    $table_content.=sprintf($r_format, $table_row_string);
}while ($row=$usersset->fetch(PDO::FETCH_ASSOC));


?>
<form action="" method="post">
    <div class="table styled-table">
        <div class="row ctr">
            Данные о пользователях
        </div>
        <div class="row ctr">
            <select name="option">
                <option value="def" selected></option>
                <option value="change">изменить</option>
                <option value="delete">удалить</option>
            </select>
            <input type="submit" value="применить">
        </div>
        <div class="cthead row">
            <?php echo sprintf($ch_format ,'галочка (выбрать все)<input name="select_all" type="checkbox"/>').$headrow;?>
        </div>
        <div class="ctbody row">
            <div class="change_table">
                <?php echo $change_table_string;?>
                <div class="row"><div><?php echo (!empty($change_table_string)) ? '<input type="submit" name="save_changes" value="сохранить изменения">': '' ;?></div></div>
            </div>
            <div class="col">
                 <?php echo $table_content;?>
            </div>
        </div>
    </div>
</form>


